import React,{ useState, useEffect } from 'react'
import Modal from '../components/Modal'
import '../style.css'

const HorasExtra=(props)=>{
    //Modal
    const[ show, setShow ] = useState('')

    const[ Fecha, getFecha ] = useState('')
    const[ Labor, getLabor ] = useState('')
    const[ Observaciones, getObservaciones ] = useState('')
    const[ Justificacion, getJustificacion ] = useState('')
    const[ Trabajadores, getTrabajadores ] = useState([])

    const handlerSubmit = props =>{
        const SolicitudCompleta ={
            Fecha,
            Labor,
            Observaciones,
            Justificacion,
            Trabajadores
        }
        
    }
    const handleTrabajadores=e=>{
        
    }
    const showModal = props =>{
        return(
            <Modal onSubmit = {handleTrabajadores(getTrabajadores)} />
        )
    }
    const handleSumit = () =>{
        
    }
    return(
        <div>
            <form onSubmit={ handleSumit }>
                <section className="FieldHorasExtra">
                    <label>Fecha Labor:
                        <input type='date' name={ Fecha } onChange={ e => getFecha(e.target.value) } />
                    </label>
                    <label> Labor a realizar 
                        <input type='text' name={ Labor } onChange={ e => getLabor(e.target.value)} />
                    </label>
                    <label> Observaciones:
                        <input type='text' name={ Observaciones } onChange={ e => getObservaciones(e.target.value)} />
                    </label>
                    <label> Justificacion:
                        <input type='text' name={ Justificacion } onChange={ e => getJustificacion(e.target.value)} />
                    </label>
                    <button onClick={ showModal }>
                        Seleccionar Trabajadores
                    </button>
                </section>
            </form>
        </div>
    )
}

export default HorasExtra