import React,{ useEffect, useState } from 'react'

const NuevoEmpleado = (props) =>{
   
    const [Nombres, getNombres] = useState('')
    const [ApellidoPaterno, getApellidoPaterno] = useState('')
    const [ApellidoMaterno, getApellidoMaterno] = useState('')
    const [Rut, getRut] = useState('')
    const [FechaIngreso, getFechaIngreso] = useState('')
    const [Seccion, getSeccion] = useState('')


    console.log()
    const handleSubmit = e =>{
        e.preventDefault()
        const persona = {
            
        }
        e.target.reset()
    }
    useEffect (()=>{
         
    })
    
    return(
        <form onSubmit={handleSubmit}>
            <h2>Ingreso de nuevo Trabajador</h2>
            <lavel> Nombres: 
                <input type="text" name={ Nombres } onChange={e => getNombres(e.target.value)} />
            </lavel>
            
            <label> Apellido Paterno:
                <input type="text" name={ ApellidoPaterno } onChange={ e =>getApellidoPaterno(e.target.value)} />
            </label>
            <label> Apellido Materno:
                <input type="text" name={ ApellidoMaterno } onChange={ e => getApellidoMaterno(e.target.value)} />
            </label>
            <br/>
            <div style={{
                position:'relative',
                left:'10px',
                top:'50px'
            }}>
            <lavel> Rut:
                <input type="text" name={ Rut } conchange={ e => getRut(e.target.value) } />
            </lavel>
            <label> Fecha Ingreso:
                <input type="date" name={ FechaIngreso } onChange={e => getFechaIngreso(e.target.value)} />
            </label>
            <label> Seccion:
                <select name={Seccion} onChange={ e => getSeccion(e.target.value)}>
                    <option defaultChecked selected name>----------------|-----------------</option>
                    <option name='Administracion' value='01'>Administracion</option>
                    <option name='Choferes' value='02'>Choferes</option>
                    <option name='Inyectado' value='03'>Inyectado</option>
                    <option name='Choferes' value='04'>Poliuretano</option>
                    <option name='Poliuretano' value='05'>Bodega-Terminacion</option>
                    <option name='Iso' value='06'>ISO</option>
                    <option name='Mangas' value='07'>Mangas</option>
                    <option name='Mantencion' value='08'>Mantencion</option>
                    <option name='Mezclado' value='10'>Mezclado</option>
                    <option name='Prensas' value='11'>Prensas</option>
                    <option name='Ventas' value='14'>Ventas</option>
                    <option name='Pintado' value='15'>Pintado</option>
                    <option name='Laboratorio' value='16'>Laboratorio</option>
                    <option name='Vendedor Zona Norte' value='17'>Vendedor Zona Norte</option>
                    <option name='Ingenieria' value='18'>Ingenieria</option>
                    <option name='Computacion' value='19'>Computacion</option>
                </ select>
            </label>
            </div>
            
            <input type="submit" name="enviar" value="Ingresar"/>
            
        </form>
    )
}

export default NuevoEmpleado 