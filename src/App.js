import React from 'react';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom'
import * as vistas from './views'
import './App.css';
import '../src/style.css'


const rutas = {
  Dashboard:'/Dashboard',
  HorasExtra:'/horasExtra',
  nuevoEmpleado:'/nuevoEmpleado',
  historial:'/historial'
}

function App(props) {
  console.log(props)
  return (
    <div className="App">
      <BrowserRouter>
      <nav>
          <button><Link className="botones" to={rutas.Dashboard}>Dashboard</Link></button>
          <button><Link className="botones" to={rutas.historial}>Historial</Link></button>
          <button><Link className="botones" to={rutas.HorasExtra}>Solicitar Horas Extra</Link></button>
          <button><Link className="botones" to={rutas.nuevoEmpleado}>nuevoEmpleado</Link></button>
      </nav>
        <Switch>
          <Route exact path={rutas.Dashboard} component={vistas.Dashboard}/>
          <Route exact path={rutas.historial} component={vistas.Historial}/>
          <Route exact path={rutas.HorasExtra} component={vistas.HorasExtra} />
          <Route exact path={rutas.nuevoEmpleado} component={vistas.NuevoEmpleado} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
